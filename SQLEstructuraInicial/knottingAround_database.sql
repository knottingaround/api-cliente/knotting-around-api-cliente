drop database if exists knottingaround_bd;
create database knottingaround_bd;
use knottingaround_bd;
DROP TABLE IF EXISTS usuarios;
CREATE TABLE usuarios(
id int not null AUTO_INCREMENT,
nombre varchar(255) not null,
passw varchar(255) not null,
apodo varchar(255),
celular varchar(25) not null unique,
correo varchar(255) not null unique,
primary key(id)
);
DROP TABLE  if exists categorias;
CREATE TABLE categorias(
id int not null AUTO_INCREMENT,
nombre varchar(255) not null,
primary key(id)
);

DROP TABLE  if exists amigurumis;
CREATE TABLE amigurumis(
id int not null AUTO_INCREMENT,
nombre varchar(255) not null,
descr varchar(255),
stock int not null,
precio decimal(8,2) not null,
descuento decimal(2,2) not null,
cate_id int not null,
estado boolean not null,
primary key(id)
);
DROP TABLE  if exists ventas;
CREATE TABLE ventas(
id int not null AUTO_INCREMENT,
id_usuario int not null,
comentario varchar(255),
fecha_solicitud timestamp not null,
fecha_aprobacion timestamp,
total decimal(8,2) not null,
estado boolean not null,
primary key(id)
);
DROP TABLE  if exists detalle_venta;
CREATE TABLE detalle_venta(
id_venta int not null,
id_amigurumi int not null,
precio decimal(8,2) not null,
descuento decimal(2,2) not null,
cantidad int
);
ALTER TABLE amigurumis
ADD FOREIGN KEY (cate_id) REFERENCES categorias(id);
ALTER TABLE ventas
ADD FOREIGN KEY (id_usuario) REFERENCES usuarios(id);
ALTER TABLE detalle_venta
ADD FOREIGN KEY (id_venta) REFERENCES ventas(id);
ALTER TABLE detalle_venta
ADD FOREIGN KEY (id_amigurumi) REFERENCES amigurumis(id);