use knottingaround_bd;
insert into categorias(nombre)  values('Anime');
insert into categorias(nombre)  values('Animales');
insert into categorias(nombre)  values('VideoJuegos');
insert into categorias(nombre)  values('Personas');
insert into categorias(nombre)  values('Caricaturas');
#amigurumis de la categoria Anime
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Rengoku',5,'Este es un amigurumi de prueba',66,0.00,true,1);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Tanjiro',5,'Este es un amigurumi de prueba',45,0.00,true,1);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Inosuke',5,'Este es un amigurumi de prueba',45,0.00,true,1);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Zenitsu',5,'Este es un amigurumi de prueba',45,0.00,true,1);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Nezuko',5,'Este es un amigurumi de prueba',50,0.00,true,1);
#amigurumis de la categoria ANIMALES
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Elefante',5,'Este es un amigurumi de prueba',50,0.00,true,2);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Tigre',5,'Este es un amigurumi de prueba',50,0.00,true,2);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Oso peresozo',5,'Este es un amigurumi de prueba',50,0.00,true,2);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Ornitorrinco',5,'Este es un amigurumi de prueba',50,0.00,true,2);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Tortuga',5,'Este es un amigurumi de prueba',50,0.00,true,2);
#amigurumis de la categoria Videojuegos
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Liu Kang',5,'Este es un amigurumi de prueba',50,0.00,true,3);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Ryu',5,'Este es un amigurumi de prueba',50,0.00,true,3);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Ken',5,'Este es un amigurumi de prueba',50,0.00,true,3);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Chun li',5,'Este es un amigurumi de prueba',50,0.00,true,3);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Crash Bandicoot',5,'Este es un amigurumi de prueba',50,0.00,true,3);
#amigurumis de la categoria Personas
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Harry Styles',5,'Este es un amigurumi de prueba',50,0.00,true,4);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('BTS',5,'Este es un amigurumi de prueba',50,0.00,true,4);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Miley Cyrus',5,'Este es un amigurumi de prueba',50,0.00,true,4);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Personalizado',5,'Este es un amigurumi de prueba',50,0.00,true,4);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Emilia Clarke',5,'Este es un amigurumi de prueba',50,0.00,true,4);
#amigurumis de la categoria Caricaturas
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Aang',5,'Este es un amigurumi de prueba',50,0.00,true,5);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Katara',5,'Este es un amigurumi de prueba',50,0.00,true,5);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Zuko',5,'Este es un amigurumi de prueba',50,0.00,true,5);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Soka',5,'Este es un amigurumi de prueba',50,0.00,true,5);
INSERT INTO amigurumis (nombre,stock,descr,precio,descuento,estado,cate_id) values('Toph',5,'Este es un amigurumi de prueba',50,0.00,true,5);

