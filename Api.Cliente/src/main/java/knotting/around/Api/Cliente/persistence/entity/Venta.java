package knotting.around.Api.Cliente.persistence.entity;
//CREATE TABLE ventas(
//        id int not null AUTO_INCREMENT,
//        id_usuario int not null,
//        comentario varchar(255),
//        fecha_solicitud timestamp not null,
//        fecha_aprobacion timestamp,
//        total decimal(8,2) not null,
//        estado boolean not null,
//        primary key(id)
//        );
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "ventas")
public class Venta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    private String comentario;
    @Column(name = "fecha_solicitud")
    private LocalDateTime fechaSolicitud;
    @Column(name = "fecha_aprobacion")
    private LocalDateTime fechaAprobacion;
    private Double total;
    private Boolean estado;

    @JoinColumn(name = "id_usuario",insertable = false,updatable = false)
    @ManyToOne
    private Cliente cliente;

    @OneToMany(mappedBy = "venta")
    private List<DetalleVenta> amigurumis;

    public List<DetalleVenta> getAmigurumis() {
        return amigurumis;
    }

    public void setAmigurumis(List<DetalleVenta> amigurumis) {
        this.amigurumis = amigurumis;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public LocalDateTime getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(LocalDateTime fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public LocalDateTime getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(LocalDateTime fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
