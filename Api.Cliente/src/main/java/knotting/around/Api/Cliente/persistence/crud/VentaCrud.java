package knotting.around.Api.Cliente.persistence.crud;

import knotting.around.Api.Cliente.persistence.entity.Venta;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface VentaCrud extends CrudRepository<Venta,Integer> {
    Optional<List<Venta>> findByIdUsuario(int idUsuario);
}
