package knotting.around.Api.Cliente.web.controller;

import knotting.around.Api.Cliente.domain.dto.DtoCategoria;
import knotting.around.Api.Cliente.domain.service.DtoCategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {
    @Autowired
    DtoCategoriaService dtoCategoriaService;
    @GetMapping("/listar")
    public ResponseEntity<List<DtoCategoria>> getAll(){
        return new ResponseEntity<>(dtoCategoriaService.getAll(), HttpStatus.OK);
    }
    @GetMapping("/buscar/{id}")
    public ResponseEntity<DtoCategoria> getById(@PathVariable("id") int id){
        return dtoCategoriaService.getById(id)
                .map(dtoCategoria -> new ResponseEntity<>(dtoCategoria,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
