package knotting.around.Api.Cliente.persistence.repository;

import knotting.around.Api.Cliente.domain.dto.DtoCategoria;
import knotting.around.Api.Cliente.domain.repository.DtoCategoriaRepository;
import knotting.around.Api.Cliente.persistence.crud.CategoriaCrud;
import knotting.around.Api.Cliente.persistence.entity.Categoria;
import knotting.around.Api.Cliente.persistence.mapper.CategoriaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoriaRepository implements DtoCategoriaRepository {
    @Autowired
    CategoriaCrud categoriaCrud;
    @Autowired
    CategoriaMapper mapper;
    @Override
    public List<DtoCategoria> getAll(){
        return mapper.toDtoCategorias((List<Categoria>) categoriaCrud.findAll());
    }

    @Override
    public Optional<DtoCategoria> getById(int id) {
        return categoriaCrud.findById(id).map(categoria -> mapper.toDtoCategoria(categoria));
    }
}
