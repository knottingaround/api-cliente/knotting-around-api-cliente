package knotting.around.Api.Cliente.persistence.repository;

import knotting.around.Api.Cliente.domain.dto.DtoVenta;
import knotting.around.Api.Cliente.domain.repository.DtoVentaRepository;
import knotting.around.Api.Cliente.persistence.crud.VentaCrud;
import knotting.around.Api.Cliente.persistence.entity.Venta;
import knotting.around.Api.Cliente.persistence.mapper.VentaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class VentaRepository implements DtoVentaRepository {
    @Autowired
    VentaCrud ventaCrud;

    @Autowired
    VentaMapper mapper;
    @Override
    public Optional<List<DtoVenta>> getByUserId(int id) {
        return ventaCrud.findByIdUsuario(id).map(ventas -> mapper.toDtoVentas(ventas));
    }
    @Override
    public List<DtoVenta> getAll(){
        return mapper.toDtoVentas((List<Venta>) ventaCrud.findAll());
    }
}
