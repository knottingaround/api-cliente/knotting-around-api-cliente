package knotting.around.Api.Cliente.domain.dto;

import java.time.LocalDateTime;
import java.util.List;

public class DtoVenta {
    private Integer id;
    private String comentario;
    private LocalDateTime fechaSolicitud;
    private LocalDateTime fechaAprobacion;
    private Double total;
    private Boolean estado;
    private List<DtoDetalle> amigurumis;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public LocalDateTime getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(LocalDateTime fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public LocalDateTime getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(LocalDateTime fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public List<DtoDetalle> getAmigurumis() {
        return amigurumis;
    }

    public void setAmigurumis(List<DtoDetalle> amigurumis) {
        this.amigurumis = amigurumis;
    }
}
