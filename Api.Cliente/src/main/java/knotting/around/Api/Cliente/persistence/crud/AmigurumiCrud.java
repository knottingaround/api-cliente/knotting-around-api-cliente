package knotting.around.Api.Cliente.persistence.crud;

import knotting.around.Api.Cliente.persistence.entity.Amigurumi;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AmigurumiCrud extends CrudRepository<Amigurumi,Integer> {
    Optional<List<Amigurumi>> findByEstado(Boolean estado);
    Optional<List<Amigurumi>> findByIdCategoriaAndEstadoTrue(int idCategoria);
    Optional<Amigurumi> findByIdAndEstadoTrue(int id);
}
