package knotting.around.Api.Cliente.web.controller;

import knotting.around.Api.Cliente.domain.dto.DtoAmigurumi;
import knotting.around.Api.Cliente.domain.service.DtoAmigurumiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/amigurumi")
public class AmigurumiController {
    @Autowired
    private DtoAmigurumiService amigurumiService;

    @GetMapping("/listar")
    public ResponseEntity<List<DtoAmigurumi>> getAllTrue(){
        List<DtoAmigurumi> amigurumis = amigurumiService.getAllWithEstadoTrue().get();
        if(amigurumis.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity<>(amigurumis,HttpStatus.OK);
        }
    }
    @GetMapping("/listar/idCategoria/{id}")
    public ResponseEntity<List<DtoAmigurumi>> getByIdCategoriaAndTrue(@PathVariable("id") int id){
        List<DtoAmigurumi> amigurumis=amigurumiService.getByIdCategoriaAndEstadoTrue(id).get();
        if(amigurumis.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity<>(amigurumis,HttpStatus.OK);
        }
    }
    @GetMapping("/buscar/idAmigurumi/{id}")
    public ResponseEntity<DtoAmigurumi> getByIdAndTrue(@PathVariable("id") int id){
        return amigurumiService.getByIdAmigurumiAndEstadoTrue(id)
                .map(dtoAmigurumi -> new ResponseEntity<>(dtoAmigurumi,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
