package knotting.around.Api.Cliente.domain.service;

import knotting.around.Api.Cliente.domain.dto.DtoCliente;
import knotting.around.Api.Cliente.persistence.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class DtoClienteService  {
    @Autowired
    ClienteRepository clienteRepository;
    public Optional<DtoCliente> getById(int id) {
        return clienteRepository.getById(id);
    }

}
