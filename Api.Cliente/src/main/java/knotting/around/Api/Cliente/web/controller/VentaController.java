package knotting.around.Api.Cliente.web.controller;

import knotting.around.Api.Cliente.domain.dto.DtoVenta;
import knotting.around.Api.Cliente.domain.service.DtoVentaService;
import knotting.around.Api.Cliente.persistence.entity.Venta;
import knotting.around.Api.Cliente.persistence.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/pedidos")
public class VentaController {
    @Autowired
    DtoVentaService dtoVentaService;
    @GetMapping("/misPedidos")
    public ResponseEntity<List<DtoVenta>> getAll(){
        return new ResponseEntity<>(dtoVentaService.getAll(), HttpStatus.OK);
    }
    @GetMapping("/userId/{id}")
    public ResponseEntity<List<DtoVenta>> getByUserId(@PathVariable("id") int id){
        return dtoVentaService.getByUserId(id)
                .map(dtoVentas -> new ResponseEntity<>(dtoVentas,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
