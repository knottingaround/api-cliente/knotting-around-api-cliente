package knotting.around.Api.Cliente.domain.service;

import knotting.around.Api.Cliente.domain.dto.DtoCategoria;
import knotting.around.Api.Cliente.persistence.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DtoCategoriaService{
    @Autowired
    CategoriaRepository categoriaRepository;

    public List<DtoCategoria> getAll() {
        return categoriaRepository.getAll();
    }
    public Optional<DtoCategoria> getById(int id){
        return categoriaRepository.getById(id);
    }
}
