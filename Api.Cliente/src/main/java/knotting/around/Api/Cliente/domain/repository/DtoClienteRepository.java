package knotting.around.Api.Cliente.domain.repository;

import knotting.around.Api.Cliente.domain.dto.DtoCliente;

import java.util.Optional;

public interface DtoClienteRepository {
    Optional<DtoCliente> getById(int id);
}
