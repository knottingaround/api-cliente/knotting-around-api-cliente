package knotting.around.Api.Cliente.persistence.crud;

import knotting.around.Api.Cliente.persistence.entity.Categoria;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaCrud extends CrudRepository<Categoria,Integer> {
}
