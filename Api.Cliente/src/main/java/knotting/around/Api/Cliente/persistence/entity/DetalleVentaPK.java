package knotting.around.Api.Cliente.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DetalleVentaPK implements Serializable {
    @Column(name = "id_venta")
    private Integer idVenta;
    @Column(name = "id_amigurumi")
    private Integer idAmigurumi;

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public Integer getIdAmigurumi() {
        return idAmigurumi;
    }

    public void setIdAmigurumi(Integer idAmigurumi) {
        this.idAmigurumi = idAmigurumi;
    }
}
