package knotting.around.Api.Cliente.persistence.mapper;

import knotting.around.Api.Cliente.domain.dto.DtoCliente;
import knotting.around.Api.Cliente.persistence.entity.Cliente;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClienteMapper {
    DtoCliente toDtoCliente(Cliente cliente);
    List<DtoCliente> toDtoClientes(List<Cliente> clientes);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "passw",ignore = true)
    })
    Cliente toCliente(DtoCliente dtoCliente);
    List<Cliente> toClientes(List<DtoCliente> dtoClientes);
}
