package knotting.around.Api.Cliente.domain.repository;

import knotting.around.Api.Cliente.domain.dto.DtoVenta;

import java.util.List;
import java.util.Optional;

public interface DtoVentaRepository {
    Optional<List<DtoVenta>> getByUserId(int id);
    List<DtoVenta> getAll();
}
