package knotting.around.Api.Cliente.persistence.crud;

import knotting.around.Api.Cliente.persistence.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteCrud extends CrudRepository<Cliente,Integer> {
}
