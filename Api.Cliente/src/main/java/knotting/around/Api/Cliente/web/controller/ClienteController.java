package knotting.around.Api.Cliente.web.controller;

import knotting.around.Api.Cliente.domain.dto.DtoCliente;
import knotting.around.Api.Cliente.domain.service.DtoClienteService;
import knotting.around.Api.Cliente.persistence.entity.Cliente;
import knotting.around.Api.Cliente.persistence.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    DtoClienteService dtoClienteService;
    @GetMapping("/id/{id}")
    public ResponseEntity<DtoCliente> getUserById(@PathVariable("id") int id){
        return dtoClienteService.getById(id)
                .map(dtoCliente -> new ResponseEntity<>(dtoCliente,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
