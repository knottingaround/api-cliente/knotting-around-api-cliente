package knotting.around.Api.Cliente.persistence.mapper;

import knotting.around.Api.Cliente.domain.dto.DtoDetalle;
import knotting.around.Api.Cliente.persistence.entity.DetalleVenta;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DetalleMapper {
    @Mappings({
            @Mapping(source = "id.idAmigurumi",target="idAmigurumi")
    })
    DtoDetalle toDtoDetalle(DetalleVenta detalleVenta);
    List<DtoDetalle> toDtoDetalles(List<DetalleVenta> detalleVentas);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "venta",ignore = true),
            @Mapping(target = "amigurumi",ignore = true),
            @Mapping(target = "id.idVenta",ignore = true),
    })
    DetalleVenta toDetalleVenta(DtoDetalle detalle);
    List<DetalleVenta> toDetalleVentas(List<DtoDetalle> dtoDetalles);
}
