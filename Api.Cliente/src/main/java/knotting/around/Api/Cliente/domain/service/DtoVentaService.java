package knotting.around.Api.Cliente.domain.service;

import knotting.around.Api.Cliente.domain.dto.DtoVenta;
import knotting.around.Api.Cliente.persistence.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class DtoVentaService {
    @Autowired
    VentaRepository ventaRepository;

    public Optional<List<DtoVenta>> getByUserId(int id) {
        return ventaRepository.getByUserId(id);
    }


    public List<DtoVenta> getAll() {
        return ventaRepository.getAll();
    }
}
