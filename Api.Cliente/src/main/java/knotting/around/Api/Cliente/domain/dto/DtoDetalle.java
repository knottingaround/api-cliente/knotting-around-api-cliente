package knotting.around.Api.Cliente.domain.dto;

public class DtoDetalle {
    private int idAmigurumi;
    private Double precio;
    private Double descuento;
    private Integer cantidad;

    public int getIdAmigurumi() {
        return idAmigurumi;
    }

    public void setIdAmigurumi(int idAmigurumi) {
        this.idAmigurumi = idAmigurumi;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
