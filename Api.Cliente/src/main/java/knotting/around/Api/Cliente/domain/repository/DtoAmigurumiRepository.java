package knotting.around.Api.Cliente.domain.repository;

import knotting.around.Api.Cliente.domain.dto.DtoAmigurumi;

import java.util.List;
import java.util.Optional;

public interface DtoAmigurumiRepository {
    Optional<List<DtoAmigurumi>> getAllWithEstadoTrue();
    Optional<List<DtoAmigurumi>> getByIdCategoriaAndEstadoTrue(int idCate);
    Optional<DtoAmigurumi> getByIdAmigurumiAndEstadoTrue(int idAmig);
}
