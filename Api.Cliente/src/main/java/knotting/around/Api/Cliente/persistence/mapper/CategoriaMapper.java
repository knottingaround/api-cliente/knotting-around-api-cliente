package knotting.around.Api.Cliente.persistence.mapper;

import knotting.around.Api.Cliente.domain.dto.DtoCategoria;
import knotting.around.Api.Cliente.persistence.entity.Categoria;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoriaMapper {
     DtoCategoria toDtoCategoria(Categoria categoria);
     List<DtoCategoria> toDtoCategorias(List<Categoria> categorias);

     @InheritInverseConfiguration

     Categoria toCategoria(DtoCategoria dtoCategoria);
     List<Categoria> toCategorias(List<DtoCategoria> dtoCategorias);
}
