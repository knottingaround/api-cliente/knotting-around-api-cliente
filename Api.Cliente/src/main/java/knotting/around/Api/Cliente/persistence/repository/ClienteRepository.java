package knotting.around.Api.Cliente.persistence.repository;

import knotting.around.Api.Cliente.domain.dto.DtoCliente;
import knotting.around.Api.Cliente.domain.repository.DtoClienteRepository;
import knotting.around.Api.Cliente.persistence.crud.ClienteCrud;
import knotting.around.Api.Cliente.persistence.entity.Cliente;
import knotting.around.Api.Cliente.persistence.mapper.ClienteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ClienteRepository implements DtoClienteRepository {
    @Autowired
    ClienteCrud clienteCrud;
    @Autowired
    ClienteMapper mapper;

    public List<Cliente> getAll(){
        return (List<Cliente>) clienteCrud.findAll();
    }

    @Override
    public Optional<DtoCliente> getById(int id) {
        return clienteCrud.findById(id).map(cliente -> mapper.toDtoCliente(cliente));
    }
}
