package knotting.around.Api.Cliente.persistence.mapper;

import knotting.around.Api.Cliente.domain.dto.DtoAmigurumi;
import knotting.around.Api.Cliente.persistence.entity.Amigurumi;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = CategoriaMapper.class)
public interface AmigurumiMapper {
    DtoAmigurumi toDtoAmigurumi(Amigurumi amigurumi);
    List<DtoAmigurumi> toDtoAmigurumis(List<Amigurumi> amigurumi);

    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "estado",ignore = true)
    })
    Amigurumi toAmigurumi(DtoAmigurumi dtoAmigurumi);
    List<Amigurumi> toAmigurumis(List<DtoAmigurumi> dtoAmigurumis);
}
