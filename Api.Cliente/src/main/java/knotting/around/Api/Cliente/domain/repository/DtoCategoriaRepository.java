package knotting.around.Api.Cliente.domain.repository;

import knotting.around.Api.Cliente.domain.dto.DtoCategoria;

import java.util.List;
import java.util.Optional;

public interface DtoCategoriaRepository {
    List<DtoCategoria> getAll();
    Optional<DtoCategoria> getById(int id);
}
