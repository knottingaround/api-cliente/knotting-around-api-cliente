package knotting.around.Api.Cliente.persistence.repository;

import knotting.around.Api.Cliente.domain.dto.DtoAmigurumi;
import knotting.around.Api.Cliente.domain.repository.DtoAmigurumiRepository;
import knotting.around.Api.Cliente.persistence.crud.AmigurumiCrud;
import knotting.around.Api.Cliente.persistence.mapper.AmigurumiMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AmigurumiRepository implements DtoAmigurumiRepository {
    @Autowired
    AmigurumiCrud amigurumiCrud;
    @Autowired
    AmigurumiMapper mapper;

    @Override
    public Optional<List<DtoAmigurumi>> getAllWithEstadoTrue() {
        return amigurumiCrud.findByEstado(true).map(amigurumis -> mapper.toDtoAmigurumis(amigurumis));
    }

    @Override
    public Optional<List<DtoAmigurumi>> getByIdCategoriaAndEstadoTrue(int idCate) {
        return amigurumiCrud.findByIdCategoriaAndEstadoTrue(idCate)
                .map(amigurumis -> mapper.toDtoAmigurumis(amigurumis));
    }

    @Override
    public Optional<DtoAmigurumi> getByIdAmigurumiAndEstadoTrue(int idAmig) {
        return amigurumiCrud.findByIdAndEstadoTrue(idAmig).map(amigurumi -> mapper.toDtoAmigurumi(amigurumi));
    }
}
