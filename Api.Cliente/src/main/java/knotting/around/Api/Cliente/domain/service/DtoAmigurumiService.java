package knotting.around.Api.Cliente.domain.service;

import knotting.around.Api.Cliente.domain.dto.DtoAmigurumi;
import knotting.around.Api.Cliente.persistence.repository.AmigurumiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class DtoAmigurumiService {
    @Autowired
    AmigurumiRepository amigurumiRepository;

    public Optional<List<DtoAmigurumi>> getAllWithEstadoTrue() {
        return amigurumiRepository.getAllWithEstadoTrue();
    }


    public Optional<List<DtoAmigurumi>> getByIdCategoriaAndEstadoTrue(int idCate) {
        return amigurumiRepository.getByIdCategoriaAndEstadoTrue(idCate);
    }


    public Optional<DtoAmigurumi> getByIdAmigurumiAndEstadoTrue(int idAmig) {
        return amigurumiRepository.getByIdAmigurumiAndEstadoTrue(idAmig);
    }
}
