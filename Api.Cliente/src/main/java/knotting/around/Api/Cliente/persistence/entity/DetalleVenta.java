package knotting.around.Api.Cliente.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
//CREATE TABLE detalle_venta(
//        id_venta int not null,
//        id_amigurumi int not null,
//        precio decimal(8,2) not null,
//        descuento decimal(2,2) not null,
//        cantidad int
//        );
@Entity
@Table(name = "detalle_venta")
public class DetalleVenta {
    @EmbeddedId
    private DetalleVentaPK id;
    private Double precio;
    private Double descuento;
    private Integer cantidad;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_venta",insertable = false,updatable = false)
    private Venta venta;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_amigurumi",insertable = false,updatable = false)
    private Amigurumi amigurumi;

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Amigurumi getAmigurumi() {
        return amigurumi;
    }

    public void setAmigurumi(Amigurumi amigurumi) {
        this.amigurumi = amigurumi;
    }

    public DetalleVentaPK getId() {
        return id;
    }

    public void setId(DetalleVentaPK id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
