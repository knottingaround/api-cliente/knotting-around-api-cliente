package knotting.around.Api.Cliente.persistence.mapper;

import knotting.around.Api.Cliente.domain.dto.DtoVenta;
import knotting.around.Api.Cliente.persistence.entity.Venta;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = {DetalleMapper.class})
public interface VentaMapper {
    DtoVenta toDtoVenta(Venta venta);
    List<DtoVenta> toDtoVentas(List<Venta> ventas);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "idUsuario",ignore = true),
            @Mapping(target = "cliente",ignore = true)
    })
    Venta toVenta(DtoVenta dtoVenta);
    List<Venta> toVentas(List<DtoVenta> dtoVentas);
}
